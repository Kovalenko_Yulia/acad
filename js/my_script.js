

/* ДОБАВЛЕНИЕ АНИМАЦИИ КНОПОК ПРИ НАВЕДЕНИИ КУPСОРА*/
function animate(elem){
    var effect = elem.data("effect");
    elem.addClass("animated " + effect).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        elem.removeClass("animated " + effect);
    });
}
$(document).ready(function(){
    $(".animated").mouseenter(function(){
        animate($(this));
    });
});

/* ДОБАВЛЕНИЕ АНИМАЦИИ INPUT*/
$(".change_input input").focus(function(){
    $(this).next("span").addClass("span_active");
});
$(".change_input input").blur(function(){
    if($(this).val() === ""){
        $(this).next("span").removeClass("span_active");
    }
});

